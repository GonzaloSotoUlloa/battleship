# BattleShip

Un clásico juego de guerra en el océano.

# Descripción del problema

Como todo estudiante de la UTFSM suele estudiar gran parte de su semana, y como toda persona debe darse un tiempo de ocio, esta instancia en que el individuo descansa de sus responsabilidades puede ser perfecto. Gracias a nuestro fabuloso juego podrá utilizar este descanso para jugar partidas de BattleShip con sus amigos navales y reponer sus fuerzas de estudio de forma entretenida.

# Solución ante el problema

BattleShip tiene sus orígenes entre los soldados de la Primera Guerra mundial, los cuales dibujaban
una cuadrícula con hoja y papel ubicando sus barcos en esta, tratando de adivinar donde se
encontraban los enemigos y disparando en posiciones en que el jugador decida. Si este logra derribar toda
la flota enemiga, será el ganador del juego. En este proyecto se realizará el desarollo planteado anteriormente con ciertas modificaciones que difieren del juego original, ya que se incluyeron embarcaciones famosas de la película Piratas del Caribe y la compra de estas embarcaciones
con un saldo fijo para lograr tener mas dinámica en el juego y variando un poco de los orígenes clásicos.
Tal como se mencionó anteriormente, el juego consiste en que cada jugador cuenta con dos tableros
desplegados por consola, uno con el que se colocan los barcos y otro que marca los tiros realizados
hacia el otro jugador. Además, cada jugador cuenta con cierta cantidad de monedas para comprar sus
naves en una tienda para poder ir a la guerra.

# Requisitos de compilación o plataforma

Para compilar el código y poder correr el programa, los requisitos mínimos de su sistema operativo deben ser: 
- Microsoft Windows 95, 98, NT 4, 2000, XP, Linux.
- 8 MB de RAM con un archivo de intercambio grande.
- Procesador compatible Intel a 100 Mhz.
- 30 MB de espacio libre en el disco duro.

Mientras que los requisitos recomendados son:
- Microsoft Windows 2000, XP, Linux.
- 32 MB de RAM.
- Procesador compatible Intel a 400.
- 200 MB de espacio libre en el disco duro.

Recordar que cualquier configuración superior a estas es válida para correr el programa.

# Instalación y Ejecución del programa

Para poder compilar el código debemos utilizar un compilador de c++ de su preferencia (ej: MinGW), luego se debe clonar el repositorio para hacer uso de los archivos. Además, se dispuso de un archivo MAKEFILE, por lo que desde la terminal al correr los siguiente comandos se ejecutará el programa:

1)make ---> Compilación del programa, creando archivos .o y .exe.

2).\game.exe  ---> Ejecución del programa.

# Modo de uso

Luego de la ejecución, el programa pedirá el nombre del Jugador 1, y qué barcos desea comprar con la fortuna que este tenga. Esta etapa concluirá con la ubicación en el océano de la flota que compró, para lo que se solicitará una coordenada con el siguiente formato (Ej: A5-V/H). Para las filas se tiene desde la A-J y para las columnas desde 0-9, además de tener una dirección Vertical(V) u Horizontal(H). 
Este mismo procedimiento será solicitado para el Jugador 2, continuando con la etapa de Guerra. Para dicha etapa, cada jugador contará con dos mapas, uno donde ubicó sus barcos y otro donde se realizan los disparos. Para ello se solicitarán las coordenadas, donde si el Jugador correspondiente acertó a un barco enemigo, ganará un punto y se le marcará en el mapa de ataque con una "F". En caso contrario, se marcará con una "O". Finalmente, el primero en llegar a 7 puntos ganará.

# Librerías utilizadas

- Vector
- String
- iostream
- stdio.h
- stdlib.h

# Desarrolladores

Gonzalo Soto Ulloa | 201930532-8

Agustín Ovando Saenz | 201930544-1

Sebastían Jara Palomino | 201944515-4

Juan de Dios Larrea López | 201930512-3

Benjamín Quiroz Aranguiz | 201904509-1

# Licencia
MIT License

GNU General Public License

# Link Video

[BattleShip](https://youtu.be/8FDDsMMNNrU)
