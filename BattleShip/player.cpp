#include <stdio.h>
#include <vector>
#include <iostream>
#include <stdlib.h>
using namespace std;
#include "player.h"
#include <String>

string HolandesErrante= "Holandes Errante";
string PerlaNegra= "Perla Negra";
string YoliMoon= "Yoli Moon";
string cordenada;
string direccion;
int row, col;


player::player():ocean(10, vector<int>(10)),oceanAtack(10, vector<int>(10)){ //constructor por defecto
    flota;
    fortuna=7;
    puntos=0;
    namePlayer="";
}

void player::setNamePlayer(const string &n){
    namePlayer=n;
}
string player::getNamePlayer(){
    return namePlayer;
}
vector<Ship> player::getFlota(){
    return flota;
}
vector<vector<int>> *player::getOcean(){
    return &ocean;
}
vector<vector<int>> *player::getOceanAtack(){
    return &oceanAtack;
}
int player::getPuntos(){
    return puntos;
}
void player::setFortuna(int precio){
    fortuna-=precio;
}
int player::getFortuna(){
    return fortuna;
}
void player::addShipToFLota(){//agrega los barcos a la flota
        cout <<getNamePlayer();
        cout << " que barco desea comprar-> 1: Holandes Errante ($3), 2: Perla Negra($2), 3: Yoli Moon($1)\n";
        while(getFortuna()>0){
        int barco;
        cin>> barco;
        if (barco==1&&getFortuna()>=3)
        {
            Ship ship(3,3,HolandesErrante);
            flota.push_back (ship);
            setFortuna(3);
        }else if (barco==2&&getFortuna()>=2){
            Ship ship2(2,2,PerlaNegra);
            flota.push_back(ship2);
            setFortuna(2);
        }else if (barco==3&&getFortuna()>=1){
            Ship ship3(1,1,YoliMoon);
            flota.push_back (ship3);
            setFortuna(1);
        }else{
            cout << "No se encuentra esa Embarcacion o no tiene suficiente dinero para comprar esa embarcacion";
        }
        cout << "Le quedan: $"<< getFortuna()<<" pesos"<<endl;
    }
}
void player::drawOcean(vector<vector<int>> &ocean){
    cout << "    0    1    2    3    4    5    6    7    8    9\n";
    char fila = 'A';
    for (int i = 0; i < 10; i++)
    {
        cout << fila<<  "   ";
        fila++;
        for (int j = 0; j < 10; j++)
        {
            if (ocean[i][j]== 0)// 0: coordenada vacia
            {
                cout<<".    ";
            }
            else if(ocean[i][j]== 1){//1: si hay un barco
                cout<<"X    ";
            }
            else if(ocean[i][j]== 2){//2:Disparo realizado
                cout<<"O    ";
            }else if(ocean[i][j]== 3){//3:Se acerto el tiro
                cout<<"F    ";
            }
        }
        cout << endl;
    }
    cout << endl;
}
void player::setCoordenadaAtack(vector<vector<int>> &o){
    bool successCin = false;
    do
    {
        cout<< "\nIngrese cordenada: ";
        cin >>cordenada;
        row = cordenada[0]-65;// estas dos lineas buscan en la tabla ASCII para convertir un string a int
        col = cordenada[1]-48;// 48 es el 0 en ASCII, por lo que transforma el char en la posicion 1 en un int
        if (cordenada.size()==2)
        {
            for (int i = 0; i < 10; i++)
            {
                if (row<10||col<10){successCin = true;break;}
            }          
        }
    } while (!(successCin));
}
void player::setCoordenada(vector<vector<int>> &o){
    bool successCin = false;
    do
    {
        cout<< "\nIngrese cordenada: ";
        cin >>cordenada;
        cout<<"Ingrese direccion: ";
        cin >>direccion;
        row = cordenada[0]-65;// estas dos lineas buscan en la tabla ASCII para convertir un string a int
        col = cordenada[1]-48;// 48 es el 0 en ASCII, por lo que transforma el char en la posicion 1 en un int
        if (cordenada.size()==2)
        {
            for (int i = 0; i < 10; i++)
            {
                if (row<10||col<10){successCin = true;break;}
            }          
        }
    } while (!(successCin));
}


void player::setShips(vector<vector<int>> &o,vector<Ship> flota){// setea los barcos en el oceano
    for (int i = 0; i <flota.size() ; i++){
        bool success = false;
        do
        {
            cout << "Coordenadas y direccion donde desea ubicar al barco (Ejemplo: A1-V/H) "<< flota[i].getName()<<": ";
            setCoordenada(o);
            int largo= flota[i].getLargo();
            if (o[row][col]==0) //comprueba si la coordenada esta vacia
            {
                if (direccion=="V")
                {
                    if (row+largo-1<10) //Borde en la filas
                    {
                        if (o[row+1][col]==0&&o[row+2][col]==0&&largo==3)// Evita la colision con otros barcos con un largo de 3
                        {
                            o[row][col]=1;
                            o[row+1][col]=1;
                            o[row+2][col]=1;
                            success=true;
                            break;
                        }
                        else if (o[row+1][col]==0&&largo==2)
                        {
                            o[row][col]=1;
                            o[row+1][col]=1;
                            success=true;
                            break;
                        }else if(o[row][col]==0&&largo==1){
                            o[row][col]=1;
                            success=true;
                            break;
                        }
                    }else
                    {
                        cout<< "\nEste barco cayo al borde del mar, reubique: "<< flota[i].getName()<<"!!!!!\n";
                    }
                    
                }
                else if(direccion=="H"){
                    if (col+largo-1<10) //Borde en la columna
                    {
                        if (o[row][col+1]==0&&o[row][col+2]==0&&largo==3)// Evita la colision con otros barcos con un largo de 2
                        {
                            o[row][col]=1;
                            o[row][col+1]=1;
                            o[row][col+2]=1;
                            success=true;
                            break;
                        }
                        else if (o[row+1][col]==0&&largo==2)
                        {
                            o[row][col]=1;
                            o[row][col+1]=1;
                            success=true;
                            break;
                        }else if(o[row][col]==0&&largo==1){
                            o[row][col]=1;
                            success=true;
                            break;
                        }
                    }
                    else
                    {
                        cout<< "\nEste barco cayo al borde del mar, reubique: "<< flota[i].getName()<<"!!!!!\n";
                    }
                }
                else
                {
                    success=false;
                }
            }else
            {
                cout<<"\n Ya existe un barco en esta posicion";
            }
            
        } while (!(success));
    }
}
void player::Shooting(vector<vector<int>> &oAtack, vector<vector<int>> &oEnemy){// En el ocean establecera las coordenadas para disparar y se realizara el disparo
    drawOcean(oAtack);
    cout << "Coordenadas donde desea Disparar (Ejemplo: A1): ";
    setCoordenadaAtack(oAtack);
    if (oEnemy[row][col]==1)// Busca si donde disparo hay un barco enemigo
    {
        puntos++;
        cout << "Hit!\n";
        oAtack[row][col]=3;
        oAtack[row][col]=3;
    }else
    {
        cout << "Miss!\n";
        oAtack[row][col]=2;

    }
}