#include <stdio.h>
#include <vector>
#include <iostream>
#include <stdlib.h>
using namespace std;

class Ship
{
    private:
        int precio,largo;
        string name;
    public:
        Ship();
        Ship(int p, int l, string &n);
        string getName();
        int getPrecio();
        int getLargo();
        friend class player;
};