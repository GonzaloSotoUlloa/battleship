#include <stdio.h>
#include <vector>
#include <iostream>
#include <stdlib.h>
using namespace std;
#include "Ship.h"
#include <String>


class player
{
    private:
        vector<Ship> flota;
        vector<vector<int>> ocean;// el mapa donde estaran sus barcos
        vector<vector<int>> oceanAtack; // mapa donde realizara los ataques
        string namePlayer;
        int fortuna;
        int puntos;
    public:
        player();
        player(const string &n);
        void setFortuna(int precio);
        vector<vector<int>> *getOcean();
        vector<vector<int>> *getOceanAtack();
        int getPuntos();
        int getFortuna();
        string getNamePlayer();
        vector<Ship> getFlota();
        void addShipToFLota();
        void setNamePlayer(const string &n);
        void drawOcean(vector<vector<int>> &o);
        void setCoordenada(vector<vector<int>> &o); // setea la coordenada de los barcos
        void setCoordenadaAtack(vector<vector<int>> &o);
        void setDireccion();
        void setShips(vector<vector<int>> &o, vector<Ship> flota); //Al colocar los barcos, la coordenada se actualiza
        void Shooting(vector<vector<int>> &oAtack,vector<vector<int>> &oEnemy);
};