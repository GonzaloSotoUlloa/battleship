#include <stdio.h>
#include <vector>
#include <iostream>
#include <stdlib.h  >
using namespace std;
#include "player.h"

void menu();

int main(){
    menu();

    string nombreP1,nombreP2;
    cout << "Nombre P1: ";
    cin >> nombreP1;
    player p1;
    p1.setNamePlayer(nombreP1);
    p1.drawOcean(*p1.getOcean());
    p1.addShipToFLota();
    p1.setShips(*p1.getOcean(),p1.getFlota());
    p1.drawOcean(*p1.getOcean());
    cout << "Nombre P2: ";
    cin >> nombreP2;
    player p2;
    p2.setNamePlayer(nombreP2);
    p2.drawOcean(*p2.getOcean());
    p2.addShipToFLota();
    p2.setShips(*p2.getOcean(),p2.getFlota());
    p2.drawOcean(*p2.getOcean());
    
    int turno=0;
    cout<<"Comienza la guerra!!!!!!!!\n";
    while (p1.getPuntos()<7||p2.getPuntos()< 7){
        if (turno%2==0)//Turnos pares juega P1, impares P2
        {
            cout << "Juega: "<<p1.getNamePlayer()<<endl;
            p1.drawOcean(*p1.getOcean());
            p1.Shooting(*p1.getOceanAtack(),*p2.getOcean());
        }else if (turno%2!=0)
        {
            cout << "Juega: "<<p2.getNamePlayer()<<endl;
            p2.drawOcean(*p2.getOcean());
            p2.Shooting(*p2.getOceanAtack(),*p1.getOcean());
        }
        turno++;
    }

    if (p1.getPuntos()==7)
    {
        cout<<"El ganador es: " << p1.getNamePlayer() << " Felicidades";
    }else if(p2.getPuntos()==7){
        cout<<"El ganador es: " << p2.getNamePlayer() << " Felicidades";
    }
    return 0;
}
void menu(){
    cout << "        Bienvenido a BattleShip";
    cout << "Reglas y Como jugar\n";
    cout << "1: Cada jugador dispone de 7 monedas para comprar barcos y derrotar al enemigo\n";
    cout << "2: Se pueden comprar 3 tipos de barcos distintos, los cuales tienen su propio largo,\n";
    cout << "   A: Holandes Errante: Con un costo de 3 monedas tiene un largo de 3 casillas\n";
    cout << "   B: Perla Negra: Con un costo de 2 monedas tiene un largo de 2 casillas\n";
    cout << "   C: Yoli Moon: Con un costo de 1 monedas tiene un largo de 1 casillas\n";
    cout << "3: Debe ubicar su flota en el oceano, senalando la coordenada \n";
    cout << "4: Ahora que la defensa esta preparada, es hora del ataque, se dispone de un mapa con\n";
    cout << "   las posiciones donde desea disparar, para enviar a las profundidades del mar al enemigo\n";
    cout << "   El primero en llegar a 7 puntos ganara!!\n";
}